Source: csvjdbc
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders:
 Christopher Hoskin <mans0954@debian.org>,
 Mechtilde Stehmann <mechtilde@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 default-jdk,
 junit4,
 libjavacc-maven-plugin-java,
 maven-debian-helper
Standards-Version: 4.6.1
Vcs-Git: https://salsa.debian.org/java-team/csvjdbc.git
Vcs-Browser: https://salsa.debian.org/java-team/csvjdbc
Homepage: http://csvjdbc.sourceforge.net

Package: libcsvjdbc-java
Architecture: all
Depends: ${misc:Depends}, ${maven:Depends}
Suggests: ${maven:OptionalDepends}
Description: read-only JDBC driver that uses CSV or DBF files as database tables
 CsvJdbc is a read-only JDBC driver that uses Comma Separated Value (CSV) files
 or DBF files as database tables. It is ideal for writing data import programs
 or analyzing log files.
 .
 The driver enables a directory or a ZIP file containing CSV or DBF files to be
 accessed as though it were a database containing tables. However, as there is
 no real database management system behind the scenes, not all JDBC
 functionality is available.
